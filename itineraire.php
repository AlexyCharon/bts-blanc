<?php
require_once('data.php');

$depart = $_POST['depart'] ? $_POST['depart'] : '';
$arrivee = $_POST['arrivee'] ? $_POST['arrivee'] : '';
$resultat = []; // représentez l'itinéraire sous la forme d'un tableau de noeuds et de distance
$erreur = NULL; // Au cas où...

session_start();

// Réalisez le traitement des informations de départ et d'arrivée ici. Faites
// attention aux itinéraires nuls (point d'arrivée = point de départ).

$_SESSION['resultat'] = $resultat;
$_SESSION['erreur'] = $erreur;

header("Location: index.php");

exit;

header('HTTP/1.1 405 Method not allowed');
?>
    <!DOCTYPE html>
    <html>
        <head>
            <title>Problème détecté</title>
        </head>
        <body>
            <h1>Une erreur est apparue</h1>
            <p>Il faudrait trouver un moyen d'arranger cela...</p>
        </body>
    </html>
<?php