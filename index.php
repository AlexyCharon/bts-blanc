<?php

require_once('data.php');

session_start();

$resultat = isset($_SESSION['resultat']) ? $_SESSION['resultat'] : NULL;
$erreur = isset($_SESSION['erreur']) ? $_SESSION['erreur'] : NULL;

?>
<!DOCTYPE html>
<html>
<head>
    <title>Calcul d'itinéraires</title>
</head>
<body>
    <form method="POST" action="itineraire.php">
        <select name="depart">
            <option value="">-- Ville de départ --</option>
            <!-- Ajoutez les autres <option> ici ! -->
                <option> Midgard </option>
                <option> Asgard </option>
                <option> Vanaheim </option>
                <option> Jotunheim </option>
                <option> Niflheim </option>
                <option> Muspelheim </option>
                <option> Alfheim </option>
                <option> Nidavellir </option>
                <option> Hel </option>
                <option> Odin's Pass </option>
                <option> Ravenclaws </option>
                <option> Shadow Steps </option>
                <option> Iron Castle </option>
        </select>
        <select name="arrivee" placeholder="-- Ville d'arrivée --">
            <option value="">-- Ville d'arrivée --</option>
            <!-- Ajoutez les autres <option> ici ! -->
                <option> Midgard </option>
                <option> Asgard </option>
                <option> Vanaheim </option>
                <option> Jotunheim </option>
                <option> Niflheim </option>
                <option> Muspelheim </option>
                <option> Midgard </option>
                <option> Alfheim </option>
                <option> Nidavellir </option>
                <option> Hel </option>
                <option> Odin's Pass </option>
                <option> Ravenclaws </option>
                <option> Shadow Steps </option>
                <option> Iron Castle </option>
        </select>
        <button type="submit">Go !</button>
    </form>
    <!-- Réalisez l'affichage de l'itinéraire calculé ici -->
</body>
<?php

function calculer_itinéraire($Ville1,$Ville2) {
$Ville1 = str_replace(" ", "+", $Ville1); //Ville de départ
$Ville2 = str_replace(" ", "+", $Ville2); //Ville d'arrivée

$charger_itinéraire = simplexml_load_string($xml);
$distance = $charger_itinéraire->route->leg->distance->value;


if ($charger_itinéraire->status == "OK") {

$distance = $distance/1000;
$distance = number_format($distance, 2, ',', ' ');

return $distance;
} else {

return "0";
}
}
